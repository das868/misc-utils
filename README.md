# misc-utils

## Goal
The prupose of this repository is to make some little Linux/Unix programs or scripts I wrote for certain particular problemsets accessible for everyone out there who can perhaps make use of them.

## Ingredients
The following programs written for these purposes are included:

| name | type | purpose |
| ---- | ---- | ------- |
| fallback-script.sh | Bash script | Automatic reinstallation of my most important programs |
| compile-script | Haskell | Automatic compilation of my seminar thesis with `pdflatex` and `biber` |
| copy-script.sh | Bash script | Automatic data backup of my seminar thesis in three generations |

## Further information
The Haskell files I compile by 

    ghc --make filename.hs

which will result in an executable `filename` within the same directory.

The Bash scripts were made executable by

    chmod +x filename.sh

to enable program execution by simply typing the filename into Terminal and hitting `ENTER`.

In general, programs are run by

    ./filename arg1 arg2 ...

## Attention
If you adapt these programs just a slight bit, they can maybe help you getting stuff done quicker since you won't have to reinvent the wheel – or at least provide a helpful example.

Please note, however, that any damage caused to your computer does not lie within my responsibility, in other words, you try these programs according to *No risk, no fun*... ;-)

---

Have fun playing around and making use of this stuff!
