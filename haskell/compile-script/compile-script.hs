import System.IO
import System.Process
import Data.List
import System.Environment

-- ALL OF THE BELOW WORK!!!
{-
main = do callCommand "echo Hello World!"
          callCommand "ls -ltr"
          callCommand "lg"
-}
-- main = do callCommand "bash -c ls"
{-
main = do args <- getArgs
          mapM putStrLn args
-}
{- 
main = do args <- getArgs
          putStrLn $ head args
-}

main = do args <- getArgs
          callCommand $ "pdflatex " ++ (head args) ++ ".tex"
          callCommand $ "pdflatex " ++ (head args) ++ ".tex"
          callCommand $ "biber " ++ (head args) ++ ".bcf"
          callCommand $ "pdflatex " ++ (head args) ++ ".tex"
          callCommand $ "pdflatex " ++ (head args) ++ ".tex"
          putStrLn "\nEverything appears to have worked..."
